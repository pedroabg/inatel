package com.inatel.stockquotemanager.service;

import com.inatel.stockquotemanager.dto.StockResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class CacheService {
    @Autowired
    RestService restService;

    static  StockResponseDto[] CACHE;
    public boolean hasCache() {
        return CACHE != null;
    }

    public void setCache(StockResponseDto[] postsPlainJSON) {
        CACHE = postsPlainJSON;
    }

    public StockResponseDto[] getCache() {
        return CACHE;
    }
    @PostConstruct
    public void cacheInit(){
        CACHE = restService.getPostsPlainJSON();
    }
    public void cleanCache(){
        CACHE = null;
    }

}
