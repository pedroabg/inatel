package com.inatel.stockquotemanager.service;

import com.inatel.stockquotemanager.dto.StockQuoteDto;
import com.inatel.stockquotemanager.dto.StockResponseDto;
import com.inatel.stockquotemanager.entity.Quote;
import com.inatel.stockquotemanager.entity.Stock;
import com.inatel.stockquotemanager.repository.QuoteRepository;
import com.inatel.stockquotemanager.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
public class StockService {

    @Autowired
    StockRepository repository;
    @Autowired
    QuoteRepository quoteRepository;
    @Autowired
    RestService restService;
    @Autowired
    CacheService cacheService;


    public boolean save(StockQuoteDto dto) {
        try {
            if(checkStock(dto.getId())){
                Stock stock = checkRetrieveStock(dto.getId());
                addQuotes(dto.getQuotes(),stock);
            }else {
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }

    private boolean checkStock(String id) throws IOException {
        StockResponseDto[] response;
        if(!cacheService.hasCache()){
            cacheService.setCache(restService.getPostsPlainJSON());
        }
        response = cacheService.getCache();

        return checkStock(id,response);
    }

    private boolean checkStock(String id, StockResponseDto[] response) {
        for (int i = 0; i < response.length; i++) {
            if(response[i].getId().equals(id))
                return true;
        }
        return false;
    }

    private void addQuotes(HashMap<LocalDate, Float> quotes, Stock stock) {
        Quote quote;
        List<Quote> quoteList = new ArrayList<>();
        for (LocalDate date: quotes.keySet()) {
            quote = new Quote();
            quote.setDate(date);
            quote.setValue(quotes.get(date));
            quote.setStock(stock);
            quoteList.add(quote);
        }
        quoteRepository.saveAll(quoteList);
    }

    private Stock checkRetrieveStock(String id) {
        Stock stock = new Stock(id);

        Optional<Stock> optionalStock = repository.findById(id);
        if(optionalStock.isPresent())
            stock = optionalStock.get();
        else
            repository.save(stock);

        return stock;
    }

    public StockQuoteDto readStockQuote(String id) {
        Stock stock = new Stock(id);
        List<Quote> quoteList = new ArrayList<>();
        quoteList = quoteRepository.findByStock(stock);

        return quoteListToDto(quoteList, stock);
    }

    private StockQuoteDto quoteListToDto(List<Quote> quoteList, Stock stock) {
        StockQuoteDto dto;
        HashMap<LocalDate, Float> quotes = new HashMap<>() ;
        for (Quote quote: quoteList) {
            quotes.put(quote.getDate(),quote.getValue());
        }
        dto = new StockQuoteDto(stock.getId(), quotes);

        return dto;
    }

    public List<StockQuoteDto> readAllStockQuotes() {
        List<StockQuoteDto> quotes = new ArrayList<>();
        List<Quote> quoteList = new ArrayList<>();
        HashMap<String, StockQuoteDto> quotesMap = new HashMap<>();
        StockQuoteDto dto;

        quoteList = quoteRepository.findAllByOrderByStockAsc();

        listToMap(quoteList, quotesMap);

        for (String id: quotesMap.keySet()) {
            quotes.add(quotesMap.get(id));
        }

        return quotes;
    }

    private void listToMap(List<Quote> quoteList, HashMap<String, StockQuoteDto> quotesMap) {
        StockQuoteDto dto;
        for (Quote quote: quoteList) {
            String id = quote.getStock().getId();
            if(!quotesMap.containsKey(id)){
                dto = new StockQuoteDto(id, new HashMap<>());
                quotesMap.put(id,dto);
            }
            quotesMap.get(id).getQuotes().put(quote.getDate(),quote.getValue());
        }
    }

    public Quote save(Quote quote) {
        return  quoteRepository.save(quote);
    }
}
