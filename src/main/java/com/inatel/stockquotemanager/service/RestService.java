package com.inatel.stockquotemanager.service;

import com.inatel.stockquotemanager.dto.ServerDto;
import com.inatel.stockquotemanager.dto.StockResponseDto;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class RestService {
    private final RestTemplate restTemplate;
    static Integer PORT = 8081;
    static String LOCALHOST = "localhost";

    public RestService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public StockResponseDto[] getPostsPlainJSON() {
        String url = "http://localhost:8080/stock";
        return this.restTemplate.getForObject(url, StockResponseDto[].class);
    }

    @PostConstruct
    public void subscribe(){
        String url = "http://localhost:8080/notification";

        // create headers
        HttpHeaders headers = new HttpHeaders();
        // set `content-type` header
        headers.setContentType(MediaType.APPLICATION_JSON);
        // set `accept` header
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        // create a map for post parameters
        Map<String, Object> map = new HashMap<>();
        map.put("host", LOCALHOST);
        map.put("port", PORT);

        // build the request
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);

        // send POST request
        ResponseEntity<String> response = this.restTemplate.postForEntity (url, entity, String.class);



    }
}
