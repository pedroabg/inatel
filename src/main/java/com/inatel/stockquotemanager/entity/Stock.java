package com.inatel.stockquotemanager.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "stock")
@Getter
@Setter
public class Stock {

    @Id
    private String id;


    public Stock(String id) {
        this.id = id;
    }
    public Stock() {}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Stock)) return false;

        Stock stock = (Stock) o;

        return id.equals(stock.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
