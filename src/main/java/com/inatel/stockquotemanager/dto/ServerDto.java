package com.inatel.stockquotemanager.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
public class ServerDto implements Serializable {
    String host;
    String port;


}
