package com.inatel.stockquotemanager.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashMap;

@Setter
@Getter
@NoArgsConstructor
public class StockResponseDto implements Serializable {
    String id;
    String description;


}
