package com.inatel.stockquotemanager.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

@Setter
@Getter
public class StockQuoteDto {
    String id;
    HashMap<LocalDate, Float> quotes;

    public StockQuoteDto(String id, HashMap<LocalDate, Float> quotes) {
        this.id = id;
        this.quotes = quotes;
    }
}
