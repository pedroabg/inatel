package com.inatel.stockquotemanager.rest;

import com.inatel.stockquotemanager.dto.StockQuoteDto;
import com.inatel.stockquotemanager.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;


@RestController
@RequestMapping(value = "/stockquotes")
public class StockController {

    @Autowired
    StockService service;

    @GetMapping("")
    public List<StockQuoteDto> getAll(){
        return service.readAllStockQuotes();
    }
    @GetMapping("/{id}")
    @ResponseStatus(code = OK)
    public StockQuoteDto getById(@PathVariable String id){
        return service.readStockQuote(id);
    }

    @PostMapping("")
    public ResponseEntity createStockQuote(@RequestBody StockQuoteDto dto){

        return service.save(dto) ? new ResponseEntity(HttpStatus.CREATED) : new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);

    }

}
