package com.inatel.stockquotemanager.rest;

import com.inatel.stockquotemanager.service.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/stockcache")
public class CacheController {

    @Autowired
    CacheService service;

    @DeleteMapping("")
    public void deleteCache(){
        service.cleanCache();
    }
}
