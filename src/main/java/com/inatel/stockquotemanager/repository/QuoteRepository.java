package com.inatel.stockquotemanager.repository;

import com.inatel.stockquotemanager.entity.Quote;
import com.inatel.stockquotemanager.entity.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface QuoteRepository extends JpaRepository<Quote, Integer> {

    List<Quote> findByStock(Stock stock);

    List<Quote> findAllByOrderByStockAsc();
}
