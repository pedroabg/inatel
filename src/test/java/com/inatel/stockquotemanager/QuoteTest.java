package com.inatel.stockquotemanager;

import com.inatel.stockquotemanager.entity.Quote;
import com.inatel.stockquotemanager.entity.Stock;
import com.inatel.stockquotemanager.service.StockService;
import static org.assertj.core.api.Assertions.assertThat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.junit.jupiter.api.Test;
import java.time.LocalDate;


@SpringBootTest
public class QuoteTest {

    @Autowired
    StockService service;

    @Test
    public void savedQuote() {
        Quote quote = new Quote();
        quote.setStock(new Stock("VIV4"));
        quote.setDate(LocalDate.now());
        Quote savedquote = service.save(quote);

        assertThat(savedquote.getId()).isNotNull();

    }
}
